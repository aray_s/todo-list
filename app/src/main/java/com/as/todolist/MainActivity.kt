package com.`as`.todolist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.`as`.todolist.adapter.ListAdapter
import com.`as`.todolist.databinding.ActivityMainBinding
import com.`as`.todolist.databinding.DialogBinding
import com.`as`.todolist.db.DatabaseHelper
import com.`as`.todolist.model.TodoList

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var listAdapter: ListAdapter
    private val database: DatabaseHelper by lazy { DatabaseHelper(this) }
    private val todoLists: MutableList<TodoList> by lazy { database.getAllLists() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        title = "Dashboard"

        listAdapter = ListAdapter(
            context = this,
            values = todoLists,
            onItemEdit = { database.updateList(it) },
            onItemDelete = { database.deleteList(it) }
        )
        binding.recyclerView.adapter = listAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this)

        binding.settings.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    SettingsActivity::class.java
                )
            )
        }
        binding.fab.setOnClickListener { addListDialog() }
    }

    private fun addListDialog() {
        val binding = DialogBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this).apply { setView(binding.root) }
        val dialog = builder.create()

        binding.dialogTitle.text = "Add ToDo List"

        binding.cancelButton.setOnClickListener { dialog.dismiss() }

        binding.addButton.setOnClickListener {
            val editText = binding.editText
            val name = editText.text.toString()

            if (name.isBlank()) editText.error = "The field cannot be empty"
            else {
                val todoList = database.addList(name)
                todoLists.add(todoList)
                listAdapter.notifyItemInserted(todoLists.lastIndex)
                dialog.dismiss()
            }
        }
        dialog.show()
    }
}