package com.`as`.todolist.model

data class TodoItem(
    val id: Long,
    var name: String,
    var completed: Boolean
)