package com.`as`.todolist.model

data class TodoList(
    val id: Long,
    var name: String
)