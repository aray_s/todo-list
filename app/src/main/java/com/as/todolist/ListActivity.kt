package com.`as`.todolist

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.`as`.todolist.adapter.ItemAdapter
import com.`as`.todolist.databinding.ActivityListBinding
import com.`as`.todolist.databinding.DialogBinding
import com.`as`.todolist.db.DatabaseHelper
import com.`as`.todolist.model.TodoItem

class ListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityListBinding
    private lateinit var prefs: SharedPreferences
    private lateinit var itemAdapter: ItemAdapter
    private lateinit var todoItems: MutableList<TodoItem>
    private val database: DatabaseHelper by lazy { DatabaseHelper(this) }
    private var listId: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListBinding.inflate(layoutInflater)
        prefs = getSharedPreferences("COLOR", Context.MODE_PRIVATE)
        intent.extras?.let {
            listId = it.getLong("LIST_ID")
            todoItems = database.getAllItems(it.getLong("LIST_ID"))
            title = it.getString("LIST_NAME")
        }

        setContentView(binding.root)

        itemAdapter = ItemAdapter(
            context = this,
            values = todoItems,
            completed = prefs.getInt("COMPLETED_COLOR", getColor(R.color.green)),
            notCompleted = prefs.getInt("NOT_COMPLETED_COLOR", getColor(R.color.brown)),
            onItemEdit = { database.updateItem(it) },
            onItemDelete = { database.deleteItem(it) }
        )
        binding.recyclerView.adapter = itemAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this)

        binding.fab.setOnClickListener { showAddItemDialog() }
    }

    private fun showAddItemDialog() {
        val binding = DialogBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this).apply { setView(binding.root) }
        val dialog = builder.create()

        binding.dialogTitle.text = "Add ToDo Item"

        binding.cancelButton.setOnClickListener { dialog.dismiss() }
        binding.addButton.setOnClickListener {
            val editText = binding.editText
            val name = editText.text.toString()

            if (name.isBlank()) editText.error = "The field cannot be empty"
            else {
                val todoItem = database.addItem(name, listId)
                todoItems.add(todoItem)
                itemAdapter.notifyItemInserted(todoItems.lastIndex)
                dialog.dismiss()
            }
        }
        dialog.show()
    }
}