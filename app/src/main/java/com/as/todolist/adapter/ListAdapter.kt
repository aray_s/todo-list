package com.`as`.todolist.adapter

import android.content.Context
import android.content.Intent
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.`as`.todolist.ListActivity
import com.`as`.todolist.databinding.FragmentListBinding
import com.`as`.todolist.model.TodoList
import com.`as`.todolist.R
import com.`as`.todolist.databinding.DialogBinding


class ListAdapter(
    private val context: Context,
    private val values: MutableList<TodoList>,
    private val onItemEdit: (todoList: TodoList) -> Unit,
    private val onItemDelete: (id: Long) -> Unit
) : RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(FragmentListBinding.inflate(LayoutInflater.from(context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.name.text = item.name
        holder.modify.setOnClickListener { showPopupMenu(holder.itemView, position, item) }
        holder.itemView.setOnClickListener {
            val intent = Intent(context, ListActivity::class.java)
            intent.putExtra("LIST_ID", item.id)
            intent.putExtra("LIST_NAME", item.name)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = values.size

    private fun showPopupMenu(view: View, position: Int, todoList: TodoList) {
        PopupMenu(context, view, Gravity.END).run {
            inflate(R.menu.menu_list_popup)
            setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.editBtn -> showUpdateListDialog(todoList, position)
                    R.id.deleteBtn -> {
                        values.remove(todoList)
                        onItemDelete(todoList.id)
                        notifyItemRemoved(position)
                    }
                }
                true
            }
            show()
        }
    }

    private fun showUpdateListDialog(todoList: TodoList, position: Int) {
        val binding = DialogBinding.inflate(LayoutInflater.from(context))
        val builder = AlertDialog.Builder(context).apply { setView(binding.root) }
        val dialog = builder.create()

        binding.addButton.text = "UPDATE"

        binding.dialogTitle.text = "Update ToDo List"
        binding.editText.append(todoList.name)

        binding.cancelButton.setOnClickListener { dialog.dismiss() }
        binding.addButton.setOnClickListener {
            val editText = binding.editText
            val name = editText.text.toString()

            if (name.isBlank()) editText.error = "List name cannot be empty"
            else {
                todoList.name = name
                onItemEdit(todoList)
                notifyItemChanged(position)
                dialog.dismiss()
            }
        }

        dialog.show()
    }

    inner class ViewHolder(binding: FragmentListBinding) : RecyclerView.ViewHolder(binding.root) {
        val name = binding.name
        val modify = binding.modify
    }
}