package com.`as`.todolist.db

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.`as`.todolist.model.TodoItem
import com.`as`.todolist.model.TodoList

@SuppressLint("Range")
class DatabaseHelper(
    context: Context
) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val queryCreateListsTable = "CREATE TABLE $TABLE_LISTS(" +
                "$KEY_LIST_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$KEY_LIST_NAME TEXT)"

        val queryCreateItemsTable = "CREATE TABLE $TABLE_ITEMS(" +
                "$KEY_ITEM_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$KEY_ITEM_NAME TEXT, " +
                "$KEY_ITEM_COMPLETED INTEGER, " +
                "$KEY_ITEM_LIST_ID INTEGER, " +
                "FOREIGN KEY($KEY_ITEM_LIST_ID) REFERENCES $TABLE_LISTS($KEY_LIST_ID))"
        db.execSQL(queryCreateListsTable)
        db.execSQL(queryCreateItemsTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_LISTS")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_ITEMS")
        onCreate(db)
    }


    fun addList(name: String): TodoList {
        val db = this.writableDatabase
        val contentValues = ContentValues().apply { put(KEY_LIST_NAME, name) }
        val id = db.insert(TABLE_LISTS, null, contentValues)
        db.close()
        return TodoList(id, name)
    }

    fun addItem(name: String, listId: Long): TodoItem {
        val db = this.writableDatabase
        val contentValues = ContentValues().apply {
            put(KEY_ITEM_NAME, name)
            put(KEY_ITEM_COMPLETED, 0)
            put(KEY_ITEM_LIST_ID, listId)
        }
        val id = db.insert(TABLE_ITEMS, null, contentValues)
        db.close()
        return TodoItem(id, name, false)
    }


    fun updateList(todoList: TodoList) {
        val db = this.writableDatabase
        val contentValues = ContentValues().apply { put(KEY_LIST_NAME, todoList.name) }
        db.update(TABLE_LISTS, contentValues, "$KEY_LIST_ID = ?", arrayOf(todoList.id.toString()))
        db.close()
    }

    fun updateItem(todoItem: TodoItem) {
        val db = this.writableDatabase
        val contentValues = ContentValues().apply {
            put(KEY_ITEM_NAME, todoItem.name)
            put(KEY_ITEM_COMPLETED, if (todoItem.completed) 1 else 0)
        }
        db.update(TABLE_ITEMS, contentValues, "$KEY_ITEM_ID = ?", arrayOf(todoItem.id.toString()))
        db.close()
    }


    fun deleteList(id: Long) {
        val db = this.writableDatabase
        db.delete(TABLE_LISTS, "$KEY_LIST_ID = ?", arrayOf(id.toString()))
        db.delete(TABLE_ITEMS, "$KEY_ITEM_LIST_ID = ?", arrayOf(id.toString()))
        db.close()
    }

    fun deleteItem(id: Long) {
        val db = this.writableDatabase
        db.delete(TABLE_ITEMS, "$KEY_ITEM_ID = ?", arrayOf(id.toString()))
        db.close()
    }


    fun getAllLists(): MutableList<TodoList> {
        val todoLists = mutableListOf<TodoList>()

        val db = this.readableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TABLE_LISTS", null)
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getLong(cursor.getColumnIndex(KEY_LIST_ID))
                val name = cursor.getString(cursor.getColumnIndex(KEY_LIST_NAME))
                val list = TodoList(id, name)
                todoLists.add(list)
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()

        return todoLists
    }

    fun getAllItems(listId: Long): MutableList<TodoItem> {
        val todoItems = mutableListOf<TodoItem>()

        val db = this.readableDatabase
        val cursor =
            db.rawQuery("SELECT * FROM $TABLE_ITEMS WHERE $KEY_ITEM_LIST_ID = $listId", null)
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getLong(cursor.getColumnIndex(KEY_ITEM_ID))
                val name = cursor.getString(cursor.getColumnIndex(KEY_ITEM_NAME))
                val completed = cursor.getInt(cursor.getColumnIndex(KEY_ITEM_COMPLETED)) == 1
                val item = TodoItem(id, name, completed)
                todoItems.add(item)
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()

        return todoItems
    }


    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "TODO_LIST"

        private const val TABLE_LISTS = "TODO_LISTS"
        private const val KEY_LIST_ID = "LIST_ID"
        private const val KEY_LIST_NAME = "LIST_NAME"

        private const val TABLE_ITEMS = "TODO_ITEMS"
        private const val KEY_ITEM_ID = "ITEM_ID"
        private const val KEY_ITEM_NAME = "ITEM_NAME"
        private const val KEY_ITEM_COMPLETED = "ITEM_COMPLETED"
        private const val KEY_ITEM_LIST_ID = "LIST_ID"
    }
}