package com.`as`.todolist

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.`as`.todolist.databinding.ActivitySettingsBinding

class SettingsActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySettingsBinding
    private lateinit var prefs: SharedPreferences

    private var completed: Int = 0
    private var notCompleted: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        prefs = getSharedPreferences("COLOR", Context.MODE_PRIVATE)
        setContentView(binding.root)

        completed = prefs.getInt("COMPLETED_COLOR", getColor(R.color.green))
        notCompleted = prefs.getInt("NOT_COMPLETED_COLOR", getColor(R.color.brown))

        binding.completedColors.setOnCheckedChangeListener { _, id ->
            completed = when (id) {
                R.id.green -> getColor(R.color.green)
                R.id.blue -> getColor(R.color.blue)
                R.id.yellow -> getColor(R.color.yellow)
                else -> throw IllegalArgumentException("Unknown color")
            }
        }

        binding.notCompletedColors.setOnCheckedChangeListener { _, id ->
            notCompleted = when (id) {
                R.id.brown -> getColor(R.color.brown)
                R.id.pink -> getColor(R.color.pink)
                R.id.violet -> getColor(R.color.violet)
                else -> throw IllegalArgumentException("Unknown color")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        prefs.edit().run {
            putInt("COMPLETED_COLOR", completed)
            putInt("NOT_COMPLETED_COLOR", notCompleted)
            apply()
        }
    }
}